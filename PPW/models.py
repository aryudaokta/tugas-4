from django.db import models

class IsiJadwal(models.Model):
    nama_matkul = models.CharField(max_length=200, default='none')
    dosen_pengajar = models.CharField(max_length=200, default='none')
    jumlah_sks = models.CharField(max_length=200, default='none')
    deskripsi = models.CharField(max_length=200, default='none')
    semester = models.CharField(max_length=200, default='none')

    def __str__(self):
        return self.nama_matkul