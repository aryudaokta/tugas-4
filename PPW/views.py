from django.shortcuts import render, HttpResponseRedirect
from .models import IsiJadwal
from .forms import Input_Form

# Create your views here.
def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def more(request):
    return render(request, 'more.html')

def story1(request):
    return render(request, 'story1.html')

def savejadwal(request):
    if (request.method == 'POST'):
        data = Input_Form(request.POST)
        if data.is_valid():
            data.save()
    show = IsiJadwal.objects.all()
    args = {'show':show}
    return render(request, 'jadwal.html', args)

def isi(request):
    return render(request, 'isi.html')

def hapus(request, pk):
    hapus = IsiJadwal.objects.filter(id=pk)    
    hapus.delete()
    return HttpResponseRedirect('/liat-jadwal/')