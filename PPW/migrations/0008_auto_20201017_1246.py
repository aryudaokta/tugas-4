# Generated by Django 3.0.8 on 2020-10-17 05:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PPW', '0007_auto_20201016_2146'),
    ]

    operations = [
        migrations.AlterField(
            model_name='isijadwal',
            name='deskripsi',
            field=models.CharField(default='none', max_length=255),
        ),
        migrations.AlterField(
            model_name='isijadwal',
            name='dosen_pengajar',
            field=models.CharField(default='none', max_length=255),
        ),
        migrations.AlterField(
            model_name='isijadwal',
            name='jumlah_sks',
            field=models.CharField(default='none', max_length=255),
        ),
        migrations.AlterField(
            model_name='isijadwal',
            name='nama_matkul',
            field=models.CharField(default='none', max_length=255),
        ),
        migrations.AlterField(
            model_name='isijadwal',
            name='semester',
            field=models.CharField(default='none', max_length=255),
        ),
    ]
