# Generated by Django 3.0.8 on 2020-10-15 01:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PPW', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='isijadwal',
            name='deskripsi',
            field=models.CharField(default='none', max_length=50),
        ),
        migrations.AddField(
            model_name='isijadwal',
            name='dosen_pengajar',
            field=models.CharField(default='none', max_length=30),
        ),
        migrations.AddField(
            model_name='isijadwal',
            name='jumlah_sks',
            field=models.CharField(default='none', max_length=2),
        ),
        migrations.AddField(
            model_name='isijadwal',
            name='semester',
            field=models.CharField(default='none', max_length=15),
        ),
    ]
