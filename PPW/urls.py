from django.urls import path
from . import views

app_name = 'PPW'

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('organization/', views.more, name='more'),
    path('story1/', views.story1, name='story1'),
    path('liat-jadwal/', views.savejadwal, name='savejadwal'),
    path('isi-jadwal/', views.isi, name='isi'),
    path('hapus-jadwal/<pk>', views.hapus, name='hapus'),
]
