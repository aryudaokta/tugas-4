from django import forms
from .models import IsiJadwal

class Input_Form(forms.ModelForm):
    class Meta:
        model = IsiJadwal
        fields = [
            'nama_matkul', 'dosen_pengajar', 'jumlah_sks', 'deskripsi', 'semester'
        ]